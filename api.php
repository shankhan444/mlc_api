<?php 

//Overriding Default Timezone to sync the time with requests. 
date_default_timezone_set('Asia/Karachi');

include('db.php');

class CCB_Mobile_API {
	
	private $db; 
	
	public function __construct() {
		
		//DB Connection for serviecs 
		$this->db = new CCB_DB();
		
		//Read to Receive API Requests.
		$this->recieveRequest();
	}
	
	/* Atuhentication Function 
	/* All checks for the basic request is checked here */
	
	private function verifyRequest($apiKey) {
		if($apiKey == 'oXk1oLVG9xpbHfqxGUBJ6CzIhYJOmaJJ') {
			return true;
		}
		
		return false;
	}
	
	/* Receives API Request
	/* Rsquests are categorised according to the services */
	
	public function recieveRequest() {
		$data = $_POST;
		if(!$this->verifyRequest($data['apikey'])) {
			$this->send(
				$this->wrapResponse(400,array())
			);
		}
		
		switch($data['request']) {
			
			case 'login' :
				$this->loginUser($data);
				break;
				
			case 'areas_attendance' :
				$this->areasAttendance($data);
				break;
			
			default :
				$this->send(
					$this->wrapResponse(610 ,array(), 'Unkown Request')
				);
		}
	}
	
	/* Send response back to the client
	/* All responses are pass through this functions */
	
	public function send($data) {
		header("Content-type: application/json");
		if(!array_key_exists('data', $data))
			$data['data'] = array();	
		echo json_encode($data);
		exit();
	}
	
	/* Responses are wrapped into specified format 
	/* All response are standardized according to following pattern */
	
	private function wrapResponse($code, $data, $custom=false) {
		$response = array();
		$response['data'] = $data;
		$response['response'] = $this->errorCodes($code);
		if(!empty($custom))
			$response['response']['message'] = $custom;
			
		return $response;
	}
	
	/* Response Error Codes
	/* All responses codes are defined here. 
	/* Every API request response contain one of these code */
	private function errorCodes($code=false) {
		$codes = array(
			'200' => array(
				'code' => '200',
				'status' => 'OK',
				'message' => 'Request Served'
			),
			'400' => array( 
				'code' => '400',
				'status' => 'ERROR',
				'message' => 'Authentication Failed! Please check API Key.'
			),
			'601' => array(
				'code' => '601',
				'status' => 'ERROR',
				'message' => 'Missing Fields.'
			),
			
			'602' => array(
				'code' => '602',
				'status' => 'ERROR',
				'message' => 'Login Failed!'
			),
			
			'610' => array(
				'code' => '610',
				'status' => 'ERROR',
				'message' => 'General Error'
			)
		);
		if($code && array_key_exists($code,$codes)) 
			return $codes[$code];
			
		return $codes['610'];
	}
	
	
	/* Chcek for the required fields 
	/* Returns the array of field which are not passed in api request */
	
	private function checkRequiredFields($fields, $data) {
		$emptyFields = array();
		foreach($fields as $field) {
			if(empty($data[$field])) {
				$emptyFields[] = $field;
			}
		}
		return $emptyFields;
	}
	
	/* Empty Error message generation
	/* Accepts list of fields which are empty.
	/* Returns a message a string. */
	
	private function emptyErrorMessage($fields) {
		$message = 'Missing parameters: ' . implode(', ', $fields);
		return $message;
	}
	
	/* Sends Instant Empty Error message
	/* Accepts arrat of empty fields to generate error message. */
	private function sendEmptyError($emptyCheck) {
		if(!empty($emptyCheck)) {
			$response = array();
			$this->send(
				$this->wrapResponse( 601, $response, $this->emptyErrorMessage($emptyCheck))
			);	
		}
	}
	
	/* Login user api service */
	private function loginUser($data){
		$emptyCheck = $this->checkRequiredFields(array('username','password','imei_no'),$data);
		$this->sendEmptyError($emptyCheck);
	
		
		$query = sprintf( "select * from %s where username='%s' and password='%s'", 
						$this->db->table('users'),
						$this->db->safeVar($data['username']), 
						md5($data['password'])
				);
				
		$users = $this->db->query($query);
		
		if(mysqli_num_rows($users) > 0) {
			
			$user = mysqli_fetch_array($users);
			
			if($data['imei_no'] != $user['imei_no'])
				$this->send(
					$this->wrapResponse( 610 , array(), 'You are not allowed to used this device.')
				);	
				
			$areas = $this->getAreas($user['id']);
			$areas_modified = array();
			foreach($areas as $area) :
				$areas_modified['areas_id'][] = $area['area_id'];
				$areas_modified['areas_name'][] = $area['area_name'];
				$areas_modified['areas_wardno'][] = $area['area_wardno'];
				$areas_modified['areas_attedance_start_time'][] = $area['area_attedance_start_time'];
				$areas_modified['areas_attedance_end_time'][] = $area['area_attedance_end_time'];
			endforeach;
			$data = array(
				'user_firstname' => $user['firstname'],
				'user_lastname' => $user['lastname'],
				'user_id' => $user['id'],
				'areas' => $areas_modified
			);
			
			$this->send(
				$this->wrapResponse( 200, $data)
			);	
			
		} else {		
		
			$this->send(
				$this->wrapResponse( 602 , array())
			);	
			
		}
	}
	
	/* Get specific user areas list */
	
	private function getAreas($user_id) {
		$query = sprintf( "select * from %s where user_id=%u", 
						$this->db->table('areas'),
						$user_id
				);
				
		$areas = $this->db->query($query);
		$uareas = array();
		while($area = mysqli_fetch_array($areas)):
			$uareas[$area['id']] = array(
				'area_id' => $area['id'],
				'area_name' => $area['name'],
				'area_wardno' => $area['wardno'],
				'area_attedance_start_time' => $area['time_start'],
				'area_attedance_end_time' => $area['time_end'],
				
			);
		endwhile;
		
		return $uareas;
	}
	
	/* Get single area info */
	public function getArea($area_id) {
		$query = sprintf( "select * from %s where id=%u", 
						$this->db->table('areas'),
						$area_id
				);
				
		$areas = $this->db->query($query);
		if(mysqli_num_rows($areas) > 0) {
			$area =  mysqli_fetch_array($areas);
			return $area;
		}
		
		return false;
	}
	
	private function getUser($user_id) {
		$query = sprintf( "select * from %s where id='%u'", 
				$this->db->table('users'), 
				$user_id
		);
		$users = $this->db->query($query);
		if(mysqli_num_rows($users) > 0) {
			$user =  mysqli_fetch_array($users);
			return $user;
		}
		return false;
	}
	
	/* Areas Attendance API Service */
	private function areasAttendance($data) {
		$emptyCheck = $this->checkRequiredFields(array('area_id','total','leaves','present','image','user_id','imei_no','latitiude','longitude'),$data);
		$this->sendEmptyError($emptyCheck);
		
		$user = $this->getUser($data['user_id']);
		if($data['imei_no'] != $user['imei_no'])
			$this->send(
				$this->wrapResponse( 610 , array(), 'You are not allowed to used this device.')
			);	
		
		$area = $this->getArea($data['area_id']);
		
		//Checking if area exist
		if(!$area)
			$this->send(
				$this->wrapResponse( 610 , array(), 'Area doesn\'t found.')
			);	
		
		
		//Checking if attendance is already marked 
		
		$query = sprintf( "SELECT * FROM %s WHERE area_id=%u AND DATE(timestamp) = CURDATE()", 
						$this->db->table('areas_attendance'),
						$data['area_id']
				);
				
		$areas = $this->db->query($query);
		if(mysqli_num_rows($areas) > 0) {
			$this->send(
				$this->wrapResponse( 610 , array(), 'Attendance already marked.')
			);	
		}
		
		// Checking if its attandance can be marked at this time for specified area. 
		$current_time = date('H:i:s');
		$ctime = DateTime::createFromFormat('H:i:s', $current_time);
		$stime = DateTime::createFromFormat('H:i:s', $area['time_start']);
		$etime = DateTime::createFromFormat('H:i:s', $area['time_end']);
		if ($ctime < $stime || $ctime > $etime) {
			$this->send(
				$this->wrapResponse( 610 , array(), 'Attandance not allowed at this time.')
			);	
		}
		
		//Saving image for the attendance 
		$image = $this->saveAttendanceImage($data['image'], $data['area_id']);
		
		$absent = $data['total'] - $data['present'] - $data['leaves'];
		$query = sprintf( "insert into %s set total=%u, present=%u, absent=%u, leaves=%u, image='%s', longitude='%s', latitiude='%s', area_id=%u, user_id=%u", 
						$this->db->table('areas_attendance'),
						$data['total'],
						$data['present'],
						$absent,
						$data['leaves'],
						$image,
						$this->db->safeVar($data['longitude']),
						$this->db->safeVar($data['latitiude']),
						$data['area_id'],
						$data['user_id']
				);
				
		$areas = $this->db->query($query);
		
		$this->send(
			$this->wrapResponse( 200, array(), 'Attendance Marked.')
		);	
	}
	
	/* Save Attendance Image 
	/* Accepts base64 image format and Area ID
	/* Returns Saved Image name on the server */
	private function saveAttendanceImage($image, $area_id) {
		
		//Generating Image name and Path to save image.
		$path = getcwd() . '/attedance_images/';
		$imageName = $area_id . '_' . time() . '.jpg';
		$imagePath = $path . $imageName;
		
		$image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image));
		file_put_contents($imagePath, $image);
		
		return $imageName;
	}
}
$api = new CCB_Mobile_API();