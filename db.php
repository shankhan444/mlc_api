<?php 

class CCB_DB {
	
	private $dbServer = 'localhost';
	private $dbName = 'mlc_app';
	private $dbUser = 'root';
	private $dbPass = '';
	private $conn;
	
	public function __construct() {
		$this->conn = mysqli_connect( $this->dbServer, $this->dbUser, $this->dbPass) or die(mysqli_error($this->conn));
		mysqli_select_db($this->conn, $this->dbName) or die(mysqli_error($this->conn));
	}
	
	public function table($table) {
		$tables = array(
			'users' => 'user',
			'areas' => 'areas',
			'areas_attendance' => 'areas_attendance'
		);
		return $tables[$table];
	}
	
	public function query($sql) {
		$res = mysqli_query($this->conn, $sql) or die(mysqli_error($this->conn));
		return $res;
	}
	
	
	public function safeVar($vars) {
		if(is_array($vars))
			foreach($vars as $key => $var) :
				$vars[$key] = mysqli_real_escape_string($this->conn, $var);
			endforeach;
		else 
			 mysqli_real_escape_string($this->conn, $vars);
		return $vars;
	}
	
}